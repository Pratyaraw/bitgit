package dao;

import java.util.List;

import model.City;
import model.Login;
import model.State;
import model.User;

public interface UserDao {

	void register(User user);
	User ValidateUser(Login login);
	List<State> getStates();
	List<City> getCities(int cityId);
	List<String> getAllEmail();
	
}

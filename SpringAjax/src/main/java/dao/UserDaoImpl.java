package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import model.State;
import model.City;
import model.Login;
import model.User;

public class UserDaoImpl implements UserDao{

	@Autowired
	DataSource datasource;
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public void register(User user) {

		String sql="insert into registration values(?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(sql, new Object[]{ user.getUserName(), user.getPassword(),user.getFirstName(), user.getLastName(), user.getEmail(),user.getGender(),
				user.getLanguage(),user.getState(),user.getCity(), user.getAddress(),
				user.getPhone()});
	}

	public User ValidateUser(Login login) {
		String sql="select * from registration where username='"+ login.getUserName() +"' and password='" + login.getPassword() +"'";
		List<User> users = jdbcTemplate.query(sql, new UserMapper());
		
		return users.size() >0 ? users.get(0) :null;
	}
	
	public List<State> getStates(){
		String sql="select *from states ";
		List<State> statesList =jdbcTemplate.query(sql, new RowMapper<State>(){

			public State mapRow(ResultSet rs, int rowNum) throws SQLException {
				State states= new State();
				states.setSid(rs.getLong("sid"));
				states.setState(rs.getString("state"));
				return states;
			}
			
				
		/*return jdbcTemplate.query(sql, new ResultSetExtractor<List<State>>(){
			public List<State> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<State> states= new ArrayList<State>();
				while(rs.next()){
					State s= new State();
					s.setSid(rs.getInt(1));
					s.setState(rs.getString(2));
				}
				return states;
			}*/
			
		});
		return statesList;
	}
	public List<City> getCities(int cityId){
		String sql="select * from city where sid="+ cityId;
		List<City> cityList =jdbcTemplate.query(sql, new RowMapper<City>(){

			public City mapRow(ResultSet rs, int rowNum) throws SQLException {
				City city= new City();
				city.setCid(rs.getInt("cid"));
				city.setCity(rs.getString("city"));
				return city;
			}
			
		});
		return cityList;
	}
	public List<String> getAllEmail(){
		String sql="select email from registration";
		List<User> email =jdbcTemplate.query(sql, new BeanPropertyRowMapper<User>(User.class));
		List<String> list= new ArrayList<String>();
		for(User local: email){
			list.add(local.getEmail());
		}
			
		return list;
	}
	
	class UserMapper implements RowMapper<User>{

		public User mapRow(ResultSet rs, int i) throws SQLException {
			User user= new User();
			user.setUserName(rs.getString("userName"));
			user.setPassword(rs.getString("password"));
			user.setFirstName(rs.getString("firstName"));
			user.setLastName(rs.getString("lastName"));
			user.setEmail(rs.getString("email"));
			user.setAddress(rs.getString("address"));
			user.setPhone(rs.getString("phone"));
			
			return user;
		}
		
	}

	
}

package controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import dao.UserDao;
import model.State;
import model.User;

@Controller  //is released in spring 4
public class RegistrationController {
	
	@Autowired
	public UserDao userdao;

	@RequestMapping(value="/register" ,method= RequestMethod.GET)
	public ModelAndView showregister(HttpServletRequest req, HttpServletResponse res){
		ModelAndView mv= new ModelAndView("register");
		System.out.println("hello");
		 List<State> statesList = userdao.getStates();
		mv.addObject("user", new User());
		mv.addObject("statesList", statesList);
		return mv;
	}
	
	
	
	/*@RequestMapping(value="/registerProcess" ,method=RequestMethod.POST)
	public ModelAndView addUser(HttpServletRequest req, HttpServletResponse res,@ModelAttribute("user") User user ){
		userdao.register(user);
		return new ModelAndView("welcome","firstName", user.getFirstName());
	}
	*/
	/*
	 * using validation
	 */
	@RequestMapping(value="/registerProcess" ,method=RequestMethod.POST)
	public String  addUser(@ModelAttribute("user") @Valid User user,BindingResult bindingresult, Model model){
		if(bindingresult.hasErrors()){
			return "register";
		}
		else{
			userdao.register(user);
			model.addAttribute("welcome", user.getFirstName());
			return "welcome";
		}
		
		
		
		
	}
	
}

package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import dao.UserDao;
import model.Login;
import model.User;

@Controller
public class LoginController {

	@Autowired
	public UserDao userdao;
	
	@RequestMapping(value="/login" ,method= RequestMethod.GET)
	public ModelAndView login(HttpServletRequest req, HttpServletResponse res){
		ModelAndView mv= new ModelAndView("login");
		mv.addObject("login", new Login());
		return mv;
		
	}
	
	@RequestMapping(value="/loginProcess", method= RequestMethod.GET)
	public ModelAndView loginProcess( @ModelAttribute("login") @Valid  Login login ,BindingResult bindingresult ,Model model){
		
		if(bindingresult.hasErrors()){
			return new ModelAndView("login");
		}
		else{

			ModelAndView mv =null;
			User user = userdao.ValidateUser(login);
			
			if(null != user){
				mv = new ModelAndView("welcome");
				mv.addObject("firstName" ,user.getFirstName());
			}
			else{
				mv= new ModelAndView("login");
				mv.addObject("message", "username and password is wrong");
				
			}
			return new ModelAndView("register");
			
		}
		
	}
}

package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dao.UserDao;
import model.City;
import model.State;

@RestController
public class RestClassController {
	@Autowired    
	public UserDao userdao;
	
	 @RequestMapping(value = "/showCity", method = RequestMethod.GET ,produces=MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<List<State>> getStates() {
		 System.out.println("hello");
	        List<State> states = userdao.getStates();
	        if(states.isEmpty()){
	            return new ResponseEntity<List<State>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	        }
	        return new ResponseEntity<List<State>>(states, HttpStatus.OK);
	    }
	/*@RequestMapping(value = "/showStates", method = RequestMethod.GET)
	public String dropDown(Model model) {
		  List<State> states = userdao.getStates();
	    model.addAttribute("state", states);
	    return "showStates";
	}*/
	
	 @RequestMapping(value = "/showCity/{cityid}", method = RequestMethod.GET ,produces=MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<List<City>> getCity(@PathVariable("cityid") int cityId) {
	        List<City> city = userdao.getCities(cityId);
	        if(city.isEmpty()){
	            return new ResponseEntity<List<City>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	        }
	        return new ResponseEntity<List<City>>(city, HttpStatus.OK);
	    }
	 @RequestMapping(value="getEmails", method = RequestMethod.GET ,produces=MediaType.APPLICATION_JSON_VALUE)
	 public ResponseEntity<List<String>> fetchEmail(){
		 List<String> email= userdao.getAllEmail();
		 if(email.isEmpty()){
			 return new ResponseEntity<List<String>>(email,HttpStatus.NO_CONTENT);
			 
		 }
		 return new ResponseEntity<List<String>>(email,HttpStatus.OK);
	 }
}

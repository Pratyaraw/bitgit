<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page isELIgnored="false" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<style type="text/css">
.error{
color:red;
	
}
</style>

<body>
<form:form action="loginProcess" id="loginForm" modelAttribute="login" method="get">
<table align="center">
<tr>
<td>
<form:label path="userName"> username</form:label>
</td>
<td>
<form:input path="userName" id="userName" name="userName"/>
<form:errors path="userName" cssClass="error"></form:errors>
</td>
</tr>
<tr>
<td>
<form:label path="password"> password</form:label>
</td>
<td>
<form:input path="password" id="password" name="password"/>
<form:errors path="password" cssClass="error"></form:errors>
</td>
</tr>
<tr>
<td>
<form:button id="login" name="login"> Login</form:button>
</td>

</tr>
<tr>
                        <td></td>
                        <td><a href="index.jsp">Home</a>
                        </td>
                    </tr>
</table>
</form:form>
<table align="center">
                <tr>
                    <td style="font-style: italic; color: red;">${message}</td>
                </tr>
            </table>
</body>
</html>
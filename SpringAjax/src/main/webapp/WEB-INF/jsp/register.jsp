<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
  <%@ page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- <script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script language="Javascript" src="./resources/js/jquery.js"></script>
     -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<!-- <script type="text/javascript">
	function showCity()() {
		alert("hi");
		$.ajax({
			type : 'GET',
			url : '/showStates',
			dataType : 'json',
			data : {
				state : state
			},
			success : function(data) {
				alert(data);
			},
			error : function(data) {
				alert("failed");
			}
		});
	}
	</script> -->
	 <script type="text/javascript">
      $(document).ready(function(){
    	  $('#state').change(function(event){
    		  var cityid=$('select#state').val();
    		  alert(cityid);
    		  $.ajax({
    			  type : 'GET',
    				url : "showCity/"+ cityid,
    				dataType : 'json',
    		  success:function(response)
    		  {
    			 var select =$('#city');
    			  select.find('option').remove();
    			  $.each(response,function(index,value){
    				  $("#city").append($("<option></option").val('value',index).html(value.city))
    				//  $('<option>').val(value).text(value).appendTo(select);
    			  });
    		  },
    		  error: function(){
    			  alert("error");
    		  }
    		  });
    	  });	
      });
      </script>
       <script type="text/javascript">
       $(document).ready(function(){
    	   $("a").focusout(function(){
    		   
    	  
    	   var email= document.regForm.email.value;
    	   var status;
    	   $.ajax({
    		   
    		   url: "getEmails",
    		   type :"GET",
    		   dataType : "json",
    		   success : function(data){
    			   var i=0;
    			   $.each(data,function(key,value){
    				   
    				   if(value==email){
    					   status="email is already registred";
    					   i=1;
    					   
    				   }
    				   if(value!==email && i==0){
    					   status="";
    					   
    				   }
    			   });
    			   document.getElementById("useremailcheck").innerHTML =status;
    		   }
    	   });
    	   
       });
       });
       </script>
       
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration</title>
</head>
<style type="text/css">
.error{
color:red;
	
}
</style>

<body>
<form:form id="regForm"  action="registerProcess" method="post" modelAttribute="user">

<table align="center">
<tr>
<td> 
<form:label path="userName">user name </form:label>
</td>
<td>
<form:input path="userName" name="userName" id="userName"/>
<form:errors path="userName" cssClass="error"></form:errors>
</td>
</tr>
<tr>
<td>
<form:label path="password"> password</form:label>
</td>
<td>
<form:password path="password" name="password" id="password"></form:password>
<form:errors path="password" cssClass="error"></form:errors>
</td>
</tr>

<tr>
<td>
<form:label path="firstName"> First Name</form:label>
</td>
<td>
<form:input path="firstName" name="firstName" id="firstName"></form:input>
<form:errors path="firstName" cssClass="error"></form:errors>
</td>
</tr>
<tr>
<td>
<form:label path="lastName"> last name</form:label>
</td>
<td>
<form:input path="lastName" name="lastName" id="lastName"/>
<form:errors path="lastName" cssClass="error"></form:errors>
</td>
</tr>
<tr>
<td>
<form:label path="email"> Email</form:label>
</td>
<td>
<a><div id="useremailcheck" style="margin-left";color:red;>
<form:input path="email" name="email" id="email"/>
</div>
</a>
<form:errors path="email" cssClass="error"></form:errors>
</td>
</tr>
<tr>
<td>
<form:label path="gender"> Gender</form:label>
</td>
<td>
<form:radiobutton path="gender" name="gender" id="gender" value="male"/>Male
<form:radiobutton path="gender" name="gender" id="gender" value="female"/>Female
<form:errors path="gender" cssClass="error"></form:errors>
</td>
</tr>
<tr>
<tr>
<td>
<form:label path="language"> Languages</form:label>
</td>
<td>
<form:checkbox path="language" name="language" id="language" value="English"/>English
<form:checkbox path="language" name="language" id="language" value="Hindi"/>Hindi
<form:checkbox path="language" name="language" id="language" value="Regional"/>Regional
<form:errors path="language" cssClass="error"></form:errors>
</td>
</tr>
<tr>
<td>
<form:label path="state">State</form:label>
</td>
<td>
<form:select path="state" name="state" id="state" >
<form:options items="${statesList}" itemValue="sid" itemLabel="state" path="state"/>
</form:select>
</td> 
</tr>
<tr>
<td>
<form:label path="city">City</form:label>
</td>
<td>
<form:select path="city" name="city" id="city">
<form:option value=""></form:option>
</form:select>
</td> 
</tr>

<tr>
<td>
<form:label path="address">Address</form:label>
</td>
<td>
<form:input path="address" name="address" id="address"></form:input>
<form:errors path="address" cssClass="error"></form:errors>
</td>
</tr>
<tr>
<td>
<form:label path="phone">Phone</form:label>
</td>
<td>
<form:input path="phone" name="phone" id="phone"/>
<form:errors path="phone" cssClass="error"></form:errors>
</td> 
</tr>
<tr>
<tr>
<td>
<form:button id="register" name="register"> Register</form:button>
</td>
</tr>
<tr>
<td>

<a href="index.jsp">Home</a>
</td>
</tr>

</table>
</form:form>

</body>
</html>